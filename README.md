# Trabajo de OpenStack - CNVR #

# Alumnos #
Ignacio Domínguez Martínez-Casanueva
Ricardo José Ruiz Fernández
Doris Ana Sângeap

# Comandos para desplegar el escenario #


```
#!bash

sudo vnx -f openstack_tutorial.xml -v --create
sudo vnx -f openstack_tutorial.xml -v -x start-all
sudo vnx -f openstack_tutorial.xml -v -x load-img
sudo vnx_config_nat ExtNet <interfaz-hacia-Internet>
sudo vnx -f openstack_tutorial.xml -v -x create-final
```

# Heat (Orchestration) service #

Si deseamos probar el servicio Heat debemos acceder al controlador de Openstack y adquirir los credenciales:
```
#!bash

ssh root@controller
source /root/bin/<usuario_de_openstack>-openrc
```
Para poder probar el template de demostración debemos tener previamente creada una red.
```
#!bash

openstack network list
```
Posteriormente ejecutamos Heat con el template 'demo-template.yml' para generar una instancia ligera Cirros que se conecta a la red que introduzcamos como parámetro:

```
#!bash

openstack stack create -t demo-template.yml --parameter "NetID=<nombre/ID de la red>" mystack
```
Transcurrido un minuto podemos ver que se ha creado el stack de Heat:
```
#!bash

openstack stack list
```
También podemos ver la salida que imprime por pantalla el stack tras haberse construido con éxito. Esos parámetros de salida son los indicados en el template en el campo 'outputs':
```
#!bash

openstack stack output show --all mystack
```
Finalmente para eliminar el stack basta con:
```
#!bash

openstack stack delete --yes mystack
```