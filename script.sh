#!/bin/sh

echo "Opciones: \n   1: Descargar ficheros \n   2: Create + Start-all + Load-img \n   3: NAT \n   4: Create-final \n   5: Create Server S3 \n   6: Destroy (soft) \n   7: Destroy (hard)\n   8: Git \n  \n"

read opt

START=$(date +%s)

if [ $opt = "1" ]; then
   		echo 'Descargando ficheros...'
   		wget http://idefix.dit.upm.es/download/cnvr/openstack_tutorial-mitaka_4n_classic_ovs-v05-with-rootfs.tgz
		sudo vnx --unpack openstack_tutorial-mitaka_4n_classic_ovs-v05-with-rootfs.tgz 
		rm *.tgz
		rm -R openstack_tutorial-mitaka_4n_classic_ovs-v05/*.xml
		cp  *.xml openstack_tutorial-mitaka_4n_classic_ovs-v05/
		cp -R conf/ openstack_tutorial-mitaka_4n_classic_ovs-v05/
		cp -R others/ openstack_tutorial-mitaka_4n_classic_ovs-v05/
		cd openstack_tutorial-mitaka_4n_classic_ovs-v05/

elif [ $opt = "2" ]; then
	   	echo 'Create + Start-all + Load-img...'
	   	cd openstack_tutorial-mitaka_4n_classic_ovs-v05/
		sudo vnx -f openstack_tutorial.xml -v --create
		sudo vnx -f openstack_tutorial.xml -v -x start-all
		sudo vnx -f openstack_tutorial.xml -v -x load-img

elif [ $opt = "3" ]; then
	   	echo 'Configurando NAT. Introduce nombre de la red externa:'
	   	read net
	   	sudo vnx_config_nat -d ExtNet $net
	   	sudo vnx_config_nat ExtNet $net

elif [ $opt = "4" ]; then
	   	echo 'Create-final...'
		cd openstack_tutorial-mitaka_4n_classic_ovs-v05/
	   	sudo vnx -f openstack_tutorial-mitaka_4n_classic_ovs-v05/openstack_tutorial.xml -v -x create-final

elif [ $opt = "5" ]; then
	   	echo 'Creating server S3...'
		cd openstack_tutorial-mitaka_4n_classic_ovs-v05/
	   	sudo openstack_tutorial.xml -v -x add-server-s3

elif [ $opt = "6" ]; then
	   	echo 'Destroying scenario (soft)...'
		cd openstack_tutorial-mitaka_4n_classic_ovs-v05/
	   	sudo vnx -f openstack_tutorial.xml -v -x destroy-final
	   	sudo vnx -f openstack_tutorial.xml -v -x destroy-final-2


elif [ $opt = "7" ]; then
	   	echo 'Destroying scenario (hard)...'
		cd openstack_tutorial-mitaka_4n_classic_ovs-v05/
		sudo vnx -f openstack_tutorial.xml -v --destroy

elif [ $opt = "8" ]; then
	   	echo 'Nombre del commit:'
	   	read descr
		sudo rm -R openstack_tutorial-mitaka_4n_classic_ovs-v05/
		git add .
		git commit -am $descr
		git push

else
	echo 'Introduzca un número del 1 al 8'

fi

NOW=$(date +%s)
DIF=$(( $NOW-$START ))	

MINS=$(( $DIF/60 ))	
SECS=$(( $DIF - ($MINS*60) )) 

echo
echo "Tarea $opt. Tiempo: $MINS m $SECS s "
echo 
